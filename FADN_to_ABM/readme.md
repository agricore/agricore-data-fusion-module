Variables of the FADN dataset have been analysed to get a selection of those that can be linked to the current agent-parameters definition.
* File SPG.docx analyses according to all agent modules the possible utilised FADN codes to generate a synthetic population which initialises the agent. 
* The selection made is subject to changes since there is a higher bound in the FADn variables request to the organisation, and the list made is extensive in relation to previous orientations (less than 300).
* SPG_dataset notebook merges the variables selected in a single dataset extracting each of them from the dataset of the variable-type table in the FADN format.
